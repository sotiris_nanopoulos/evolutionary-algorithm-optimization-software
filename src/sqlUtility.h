#ifndef SQLUTILITY_H
#define SQLUTILITY_H

#include <string>
#include <array>
#include "objective_function.hpp"


#define _number_clearable_tables_ 4

static int callback(void *Used, int argc, char **argv, char **azColName){
    return 0;
}
namespace ea_optimizer{

    struct sql_utility{
        static void clean_previous(std::array<std::string,_number_clearable_tables_> table_names,sqlite3 *db){
            for (const auto& table:table_names){
            std::string query = "delete from `" + table + "`";
                sqlite3_exec(db, query.c_str(), callback, 0, NULL);
            }
        }
        static void  add_population_database(const std::vector<shark::Individual<shark::RealVector,shark::RealVector>>& population,
                                                const shark::objective_function& objective_function,
                                                const int generation,
                                                sqlite3 *db,
                                                const std::string& table_name){
            for( std::size_t i = 0; i < population.size(); i++ ){
                std::string query(basic_query(objective_function.numberOfObjectives(),
                                objective_function.numberOfVariables(),table_name));
                for( std::size_t j = 0; j < objective_function.numberOfObjectives(); j++ )
                    query += std::to_string(population[i].penalizedFitness()[j]) +",";
                query +=std::to_string(generation) +","+ std::to_string(objective_function.get_evaluations())+",";
                auto variables_real_values = objective_function.unregularized(population[i].searchPoint());
                for( std::size_t j = 0; j < objective_function.numberOfVariables()-1; j++ )
                    query += std::to_string(variables_real_values[j]) +",";
                query += std::to_string(variables_real_values[objective_function.numberOfVariables()-1])+")";
                sqlite3_exec(db, query.c_str(), callback, 0, NULL);
            }
        }
        static void add_pareto_database(const shark::AbstractMultiObjectiveOptimizer<shark::RealVector>& optimizer,
                                        const int number_variables,
                                        const int number_objectives,
                                        const shark::objective_function& objective_function,
                                        const int generation,
                                        sqlite3 *db){
        for (std::size_t i = 0; i < optimizer.solution().size(); i++){
        std::string query(basic_query(number_objectives,number_variables,"Pareto Front"));
        for( std::size_t j = 0; j < optimizer.solution()[i].value.size(); j++ )
            query += std::to_string(optimizer.solution()[i].value[j]) +",";
        query +=std::to_string(generation) +","+ std::to_string(objective_function.get_evaluations())+",";
        auto variables_real_values = objective_function.unregularized(optimizer.solution()[i].point);
        for( std::size_t j = 0; j < variables_real_values.size()-1; j++ )
            query += std::to_string(variables_real_values[j]) +",";
        query += std::to_string(variables_real_values[variables_real_values.size()-1])+")";
        sqlite3_exec(db, query.c_str(), callback, 0, NULL);
        }
    }
    
    private:
        static std::string basic_query(int number_objectives,
                                int number_variables,
                                const std::string& table_name){
            std::string insert_query = "insert into `" +table_name + "` (";
            insert_query += "`Objective " + std::to_string(1)+"`";
            for (int i = 2; i <= number_objectives;i++)
                insert_query += ", `Objective " + std::to_string(i)+"`";
            insert_query += ",`Generation Number`,`Calls to Objective Function`";
            for (int i = 0; i< number_variables-1; i++)
                insert_query +=", `Variable " + std::to_string(i+1)+"`";
            insert_query += ", `Variable " + std::to_string(number_variables)+"`)";
            insert_query += "values(";
            return insert_query;
        }
    };
}

#endif