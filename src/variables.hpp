//
//  variables.hpp
//  optimizer3
//
//  Created by davinci on 02/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#ifndef variables_hpp
#define variables_hpp

#include <stdio.h>
#include <vector>
#include <tuple>
#include <assert.h>
#include <memory>
#include <string>
#include <sqlite3.h>

class variables{

public:
    typedef std::vector<std::tuple<double,double,int>> bounds;
    typedef std::vector<std::tuple<int,std::string>> ansys_data;
    
    enum regularization_types{linear,exponential};
    enum bounds_indexes{lower_b,upper_b,regularization_type};
    enum ansys_data_indexes{id,name};

    variables();
    variables(sqlite3 *db);
    
    
    ansys_data& get_ansys_data() const {
        assert(ansys_info && "Description in Data Conteiner is NULL");
        return *ansys_info;
    }
    
    bounds get_variable_bounds() const{
        return variable_bounds;
    }
    
    // Copy Operator //
    variables(variables const& x){
        if (x.ansys_info)
            ansys_info = std::unique_ptr<ansys_data>(new ansys_data(*x.ansys_info));
        else
            ansys_info = nullptr;
        
        variable_bounds=x.get_variable_bounds();
    }
    
    // Equal Operator//
    const variables& operator=(variables const& x){
        
        if (x.ansys_info)
             *ansys_info = *x.ansys_info;
        else
            ansys_info = nullptr;
        
        variable_bounds = x.get_variable_bounds();
        return *this;
    }
    
    int callback_for_bounds(int argc, char **argv, char **azColName);
    int callback_for_ansys(int argc, char **argv, char **azColName);
    
private:
 
    std::unique_ptr<ansys_data> ansys_info;
    bounds  variable_bounds;


};

#endif /* variables_hpp */
