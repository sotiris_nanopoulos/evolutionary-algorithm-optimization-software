#ifndef PARSER_H
#define PARSER_H
#include <string>

class commandLineParser{
public:
    commandLineParser(int argc,const char * argv[]){
        database_location = "ZDT1.db";
        if (argc == 2)
            database_location = std::string(argv[1]);
    }
    std::string get_database_location(){return database_location;}
private:
    std::string database_location;

};

#endif