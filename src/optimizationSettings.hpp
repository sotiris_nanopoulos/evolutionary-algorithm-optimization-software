//
//  optimizationSettings.hpp
//  optimizer3
//
//  Created by davinci on 01/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#ifndef optimizationSettings_hpp
#define optimizationSettings_hpp

#include <stdio.h>
#include <sqlite3.h>
#include <string>
#include <memory.h>

class optimization_settings{

public:
    optimization_settings();
    optimization_settings(sqlite3 *db);
    
    int callback_optimization_parameters(int argc, char **argv, char **azColName);
 
    
    int number_of_objectives;
    int number_of_variables;
    int number_of_evaluations;
    int population_size;
    int generation_size;
    int optimization_method;

    
    double crossover_propability;
    double mutation_propability;
    double mutation_epsilon;
    
};




#endif /* optimizationSettings_hpp */
