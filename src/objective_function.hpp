//
//  objective_function.hpp
//  optimizer3
//
//  Created by davinci on 02/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#ifndef objective_function_hpp
#define objective_function_hpp

#include <stdio.h>
#include <sqlite3.h>
#include "variables.hpp"
#include <shark/ObjectiveFunctions/AbstractObjectiveFunction.h>
#include <shark/ObjectiveFunctions/BoxConstraintHandler.h>

namespace shark{

class objective_function: public MultiObjectiveFunction{

public:
    
    objective_function(std::size_t number_variables,std::size_t number_objectives_,
                       const variables& var,sqlite3 *db_);
    std::size_t numberOfObjectives()const{
        return number_objectives;
    }
    
    bool hasScalableObjectives()const{
        return true;
    }
    
    void setNumberOfObjectives( std::size_t numberOfObjectives ){
        number_objectives = numberOfObjectives;
    }
    
    
    std::size_t numberOfVariables()const{
        return m_handler.dimensions();
    }
    
    bool hasScalableDimensionality()const{
        return true;
    }
    
    ResultType eval( const SearchPointType & x ) const;
    
    //RealVector eval( const RealVector & x ) const;
    
    void set_generation_number(int s){generation_number=s;}

    int get_evaluations() const {return m_evaluationCounter;}

    SearchPointType unregularized(const SearchPointType& x) const;

private:
    
    bool worth_calculation(ResultType &y,const SearchPointType& x) const ;
    void update_db(const ResultType &y,const SearchPointType& x) const;
    sqlite3 *db;
    int generation_number;
    variables variables_info;
    std::string insert_query;
    std::string executable_directory;
    std::size_t number_variables;
    std::size_t number_objectives;
    BoxConstraintHandler<SearchPointType> m_handler;

};


}





#endif /* objective_function_hpp */
