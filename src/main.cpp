//
//  main.cpp
//  optimizer3
//
//  Created by davinci on 01/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//
#include <iostream>
#include <memory>
#include <array>
#include "optimizationSettings.hpp"
#include "variables.hpp"
#include "objective_function.hpp"
#include "myNSGAII.hpp"
#include "extrapolator.hpp"
#include "parseCommandLine.hpp"
#include "definitions.hpp"
#include "sqlUtility.h"
#include <shark/Algorithms/DirectSearch/Indicators/HypervolumeIndicator.h>
#include <shark/Algorithms/DirectSearch/Indicators/AdditiveEpsilonIndicator.h>

int main(int argc, const char * argv[]) {
    std::array<std::string, _number_clearable_tables_> tables {"Calculations","Pareto Front","Extrapolated Points","Debug Table"};
    auto parser = new commandLineParser(argc,argv);
    sqlite3 *db;
    int rc = sqlite3_open(parser->get_database_location().c_str(),&db);
    if (rc){
        std::cout<< "Can't open database\nProgram will be terminated"<< std::endl;
        exit(0);
    }
    optimization_settings settings(db);
    variables v(db);
    shark::objective_function objective_function(settings.number_of_variables,settings.number_of_objectives,v,db);
    shark::myNSGAII<shark::AdditiveEpsilonIndicator> optimizer;
    objective_function.init();
    optimizer.init(objective_function);
    ea_optimizer::sql_utility::clean_previous(tables,db);
    int generation=0;
    while( objective_function.evaluationCounter() < _objective_function_calls_ ) {
        std::cout << "Generation :" << generation << " ";
        objective_function.set_generation_number(generation);
        optimizer.step(objective_function);
        generation++; 
        std::cout << "Pareto Front Size: " << optimizer.solution().size() << std::endl;
        ea_optimizer::sql_utility::add_pareto_database(optimizer,settings.number_of_variables,
                                                       settings.number_of_objectives,
                                                       objective_function,
                                                       generation,db);
        if (generation%_extrapolation_interval_==0 && false){
           auto population = optimizer.get_pop();
            ea_optimizer::sql_utility::add_population_database(population,objective_function,generation,db,"Debug Table");
            extrapolator extrap(settings.number_of_variables,
                                settings.number_of_objectives,
                                _hidden_layer_neurons_,_training_type_,db);
            auto extrapolated_population = extrap.extrapolatedPoints(population,objective_function);
            ea_optimizer::sql_utility::add_population_database(extrapolated_population,objective_function,generation,db,"Extrapolated Points");
            optimizer.updatePopulation(extrapolated_population);
        }
    }
    return 0;
}
