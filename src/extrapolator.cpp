//
//  extrapolator.cpp
//  optimizer3
//
//  Created by davinci on 03/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#include "extrapolator.hpp"
#include <unordered_set>
#include <assert.h>

using namespace shark;
using namespace std;

static int variablesCallback(void *Used, int argc, char **argv, char **azColName){
    extrapolator* extr = reinterpret_cast<extrapolator*>(Used);
    return extr->callback_for_traing_data(argc, argv, azColName);
}

extrapolator::extrapolator(int number_of_variables_,int number_of_objectives_,
                           int hidden_neurons_,int training_type_, sqlite3 *db_)
                           :training_type(training_type_),db(db_),
                           number_of_variables(number_of_variables_),
                           number_of_objectives(number_of_objectives_){
 
    int total = number_of_variables+number_of_objectives;
    if (total%2==0)
        input_neurons = total/2;
    else
        input_neurons = (total-1)/2 +1;
    output_neurons=total -input_neurons;
    network.setStructure(input_neurons,hidden_neurons_,output_neurons,shark::FFNetStructures::Normal,true);
}
void extrapolator::set_data(){
    inputs.resize(0);
    labels.resize(0);
    std::string query="SELECT * from `Calculations` order by `Calls to Objective Function` desc limit " + to_string(training_set_size);
    sqlite3_exec(db, query.c_str(), variablesCallback, (void*) this, NULL);
}

void extrapolator::train(){
    set_data();
    RegressionDataset dataset = createLabeledDataFromRange(inputs,labels);
    SquaredLoss<RealVector,RealVector> loss; // surrogate loss for training
    ErrorFunction error(dataset,&network,&loss);
    //initialize Rprop and initialize the network randomly
    initRandomUniform(network,-0.1,0.1);
    IRpropPlus optimizer;
    error.init();
    optimizer.init(error);
    unsigned numberOfSteps = 1000;
    for(unsigned step = 0; step != numberOfSteps; ++step)
        optimizer.step(error);
    // evaluate solution found by training
    network.setParameterVector(optimizer.solution().point); // set weights to weights found by learning 
}

extrapolator::population extrapolator::extrapolatedPoints(const population &pop,
                                                          const objective_function& obj_function){
    population extrpolated_population;
    train();
    for( std::size_t i = 0; i < pop.size(); i=i+extrapolation_param.increment_size ){
        RealVector input_;
        auto variables_real_values = obj_function.unregularized(pop[i].searchPoint());
        for( std::size_t j = 0; j < number_of_objectives; j++ )
            input_.push_back(extrapolation_param.extrapolationFactor*pop[i].penalizedFitness()[j]);
        for( std::size_t j = 0; j < (input_neurons-number_of_objectives); j++ )
            input_.push_back(variables_real_values[j]);
        RealVector prediction = network(input_);
        RealVector extrapolated_individual;
        for( std::size_t j = 0; j < (input_neurons-number_of_objectives); j++ )
            extrapolated_individual.push_back(variables_real_values[j]);
        for (const auto &p:prediction)
            extrapolated_individual.push_back(p);
        assert(extrapolated_individual.size()==number_of_variables);
        RealVector real_extrapolated_points = obj_function.eval(extrapolated_individual);
        Individual<RealVector,RealVector> ind;
        ind.searchPoint() = extrapolated_individual;
        ind.penalizedFitness() = real_extrapolated_points;
        extrpolated_population.push_back(ind);
    }
    return extrpolated_population;
}
extrapolator::population extrapolator::extrapolatedPoints(const AbstractMultiObjectiveOptimizer<RealVector>& optimizer,
                              const shark::objective_function& obj_function){
    population extrpolated_population;
    train();



    return extrpolated_population;
}


int extrapolator::callback_for_traing_data(int argc, char **argv, char **azColName){
    unordered_set<int> invalid_collumns({0,number_of_objectives+1,number_of_objectives+2});
    RealVector input_temp;
    RealVector labels_temp;
    for (int i=0;i<argc;i++){
        if (invalid_collumns.find(i)==invalid_collumns.end()){
            if ( input_temp.size()<input_neurons)
                input_temp.push_back(atof(argv[i]));
            else
                labels_temp.push_back(atof(argv[i]));
        }
    }
   inputs.push_back(input_temp);
   labels.push_back(labels_temp);
   return 1;
}
