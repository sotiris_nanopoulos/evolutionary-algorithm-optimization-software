//
//  objective_function.cpp
//  optimizer3
//
//  Created by davinci on 02/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#include "objective_function.hpp"
#include <stdlib.h>
#include <shark/ObjectiveFunctions/AbstractObjectiveFunction.h>
#include <cmath>

static int callback(void *Used, int argc, char **argv, char **azColName){
    return 0;
}


static int search_callback(void *Used, int argc, char **argv, char **azColName){
    
    if (argc==0) return argc;
    
    shark::objective_function::ResultType* objectives =
    reinterpret_cast<shark::objective_function::ResultType*>(Used);
   
    for (auto i=0;i<objectives->size();i++)
        objectives->operator[](i) = atof(argv[i+1]);
    
    return argc;
}


shark::objective_function::objective_function(std::size_t number_variables,std::size_t number_objectives_,
                                              const variables& var,sqlite3 *db_)
:db(db_),number_objectives(number_objectives_),number_variables(number_variables),
m_handler(number_variables,-1,1),variables_info(var){
    announceConstraintHandler(&m_handler);
    insert_query = "insert into Calculations (";
    insert_query += "`Objective " + std::to_string(1)+"`";
    for (int i = 2; i <= number_objectives;i++)
        insert_query += ", `Objective " + std::to_string(i)+"`";
    insert_query += ",`Generation Number`,`Calls to Objective Function`";
    for (int i = 0; i< number_variables-1; i++)
        insert_query +=", `Variable " + std::to_string(i+1)+"`";
    insert_query += ", `Variable " + std::to_string(number_variables)+"`)";
    insert_query += "values(";
}

bool shark::objective_function::worth_calculation(ResultType &y,const SearchPointType& x) const{
    std::string query;
    query = "select * from Calculations where ";
    for (auto i=0; i < (number_variables-1);i++)
        query += "`Variable " + std::to_string(i+1)+"`" + "=" + std::to_string(x(i)) + " AND ";
    query += "`Variable " + std::to_string(number_variables) +"`" +"=" + std::to_string(x(number_variables-1));
    int rc = sqlite3_exec(db, query.c_str(), search_callback,&y, NULL);
    return rc != 0;
    
}

void shark::objective_function::update_db(const ResultType &y,const SearchPointType& x) const{
    std::string query = insert_query;
    for (int i = 0; i < number_objectives; i++)
        query += std::to_string(y[i]) + ",";
    query +=std::to_string(generation_number) + "," +std::to_string(m_evaluationCounter) +",";
    for (int i = 0; i< number_variables-1; i++)
        query += std::to_string(x[i]) + ",";
    query +=std::to_string(x[number_variables-1])+")";
    sqlite3_exec(db, query.c_str(), callback, 0, NULL);
    
}

shark::objective_function::SearchPointType shark::objective_function::unregularized(const SearchPointType& x) const{
    shark::objective_function::SearchPointType toReturn;
    variables::bounds var_bounds =variables_info.get_variable_bounds();
    toReturn.resize(var_bounds.size());
    for (int i=0;i<var_bounds.size();i++){
        toReturn(i) = x(i);
        if (toReturn(i)<-0.9999999)
            toReturn(i) = -1;
        
        if (toReturn(i)>0.9999)
             toReturn(i) = 0.9999999;
    
        double lowestLim =std::get<variables::lower_b>(var_bounds[i]);
        double upperLime =std::get<variables::upper_b>(var_bounds[i]);
        
        if (std::get<variables::regularization_type>(var_bounds[i]) == 0)
            toReturn(i) = lowestLim+ (toReturn(i)+1)/(2.*(upperLime-lowestLim));
        else{
            double bb = sqrt(1. / (lowestLim*upperLime));
            double aa = 1 / log10(upperLime*bb);
            toReturn(i) =  pow(10., x(i) / aa) / bb;
        }
    }
    return toReturn;
}
shark::objective_function::ResultType shark::objective_function::eval( const SearchPointType & x ) const {
    
    ResultType value( numberOfObjectives() );
    
    SearchPointType x_corr = unregularized(x);
    if (!worth_calculation(value,x_corr)){
        m_evaluationCounter++;
        value[0] = x_corr(0);
        double g = 1.0 + 9.0 *(sum(x_corr)-x_corr(0))/(numberOfVariables() - 1.0);
        double h = 1.0 - std::sqrt(x_corr(0) / g);
        value[1] = g * h;
        update_db(value,x_corr);
    }
    return value;
}

