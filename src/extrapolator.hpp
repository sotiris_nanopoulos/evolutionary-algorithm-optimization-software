//
//  extrapolator.hpp
//  optimizer3
//
//  Created by davinci on 03/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#ifndef extrapolator_hpp
#define extrapolator_hpp

#include <stdio.h>
#include <sqlite3.h>
#include "definitions.hpp"
#include "objective_function.hpp"
#include<shark/Models/FFNet.h> //the feed forward neural network
#include <shark/Algorithms/AbstractMultiObjectiveOptimizer.h>
#include<shark/Algorithms/GradientDescent/Rprop.h> //resilient propagation as optimizer
#include<shark/ObjectiveFunctions/Loss/CrossEntropy.h> // loss during training
#include<shark/ObjectiveFunctions/ErrorFunction.h> //error function to connect data model and loss
#include<shark/ObjectiveFunctions/Loss/SquaredLoss.h> //loss for test performance
#include <shark/Algorithms/DirectSearch/Individual.h>


struct extrapolation_parameters{
    extrapolation_parameters(){
        double extrapolationFactor = _extrapolation_factor_;
        int    increment_size= _population_extrapolation_increment_; 
    }
    double extrapolationFactor;
    int    increment_size;
};


class extrapolator{
    typedef std::vector<shark::Individual<shark::RealVector,shark::RealVector>> population;
public:
    enum training_types{online,batch};
    extrapolator(int number_of_variables_,int number_of_objectives_,
                 int hidden_neurons_,int trainging_type_, sqlite3 *db_);
    int callback_for_traing_data(int argc, char **argv, char **azColName);
    population extrapolatedPoints(const population &pop,
                                  const shark::objective_function& obj_function);

    population extrapolatedPoints(const shark::AbstractMultiObjectiveOptimizer<shark::RealVector>& optimizer,
                                  const shark::objective_function& obj_function);
private:
    void set_data();
    void train();
    shark::FFNet<shark::LogisticNeuron,shark::LogisticNeuron> network;
    std::vector<shark::RealVector> inputs;
    std::vector<shark::RealVector> labels;
    sqlite3 *db;
    extrapolation_parameters extrapolation_param;
    int number_of_variables;
    int number_of_objectives;
    int input_neurons;
    int output_neurons;
    int training_type;
    int training_set_size=300;
};








#endif /* extrapolator_hpp */
