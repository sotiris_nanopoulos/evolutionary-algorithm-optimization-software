//
//  optimizationSettings.cpp
//  optimizer3
//
//  Created by davinci on 01/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#include "optimizationSettings.hpp"
#include <stdlib.h>

static int callback(void *Used, int argc, char **argv, char **azColName){
    optimization_settings* parameters = reinterpret_cast<optimization_settings*>(Used);
    return parameters->callback_optimization_parameters(argc, argv, azColName);
}



optimization_settings::optimization_settings(){}

optimization_settings::optimization_settings(sqlite3 *db){

    int  rc;
    std::string query("SELECT * from `Optimization Parameters`");
    rc = sqlite3_exec(db, query.c_str(), callback, (void*) this, NULL);
    
}

int optimization_settings::callback_optimization_parameters(int argc, char **argv, char **azColName){
    number_of_objectives = atoi(argv[1]);
    number_of_variables = atoi(argv[2]);
    number_of_evaluations = atoi(argv[3]);
    population_size = atoi(argv[4]);
    generation_size = atoi(argv[5]);
    optimization_method = atoi(argv[6]);
    crossover_propability = atof(argv[7]);
    mutation_propability = atof(argv[8]);
    mutation_epsilon = atof(argv[9]);
    return 0;
}
