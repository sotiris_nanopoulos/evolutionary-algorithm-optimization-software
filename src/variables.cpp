//
//  variables.cpp
//  optimizer3
//
//  Created by davinci on 02/04/2017.
//  Copyright © 2017 davinci. All rights reserved.
//

#include "variables.hpp"
#include <stdlib.h>

static int variablesCallback(void *Used, int argc, char **argv, char **azColName){
    variables* var = reinterpret_cast<variables*>(Used);
    return var->callback_for_bounds(argc, argv, azColName);
}

static int ansysCallback(void *Used, int argc, char **argv, char **azColName){
    variables* var = reinterpret_cast<variables*>(Used);
    return var->callback_for_ansys(argc, argv, azColName);
}

variables::variables(sqlite3 *db){

    std::string query("SELECT * from `Variable Bounds`");
    sqlite3_exec(db, query.c_str(), variablesCallback, (void*) this, NULL);

    query = std::string("Select * from `Ansys Information`");
    sqlite3_exec(db, query.c_str(), ansysCallback,(void*) this, NULL);

}

int variables::callback_for_bounds(int argc, char **argv, char **azColName){
    variable_bounds.push_back(std::make_tuple(atof(argv[1]),atof(argv[2]),atoi(argv[3])));
    return 0;
}

int variables::callback_for_ansys(int argc, char **argv, char **azColName){
    return 0;
}

