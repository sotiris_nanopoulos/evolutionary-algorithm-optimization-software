#ifndef MY_DEFINITIONS
#define MY_DEFINITIONS

#define _objective_function_calls_ 10000

#define _hidden_layer_neurons_ 20
#define _training_type_ 0

#define _extrapolation_interval_ 3
#define _population_extrapolation_increment_ 5
#define _extrapolation_factor_ 0.45

#endif