project(optimizer_version3)
cmake_minimum_required(VERSION 2.8.8)
set(CMAKE_BUILD_TYPE Debug)

set(Boost_INCLUDE_DIR /usr/local/Cellar/boost/1.63.0/include)
set(Boost_LIBRARY_DIR /usr/local/Cellar/boost/1.63.0/lib)
# Find Boost
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)

set(CMAKE_CXX_STANDARD 11)
LINK_DIRECTORIES(${Boost_LIBRARY_DIR} ${PROJECT_SOURCE_DIR}/Libraries/Shark)
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
include_directories(${PROJECT_SOURCE_DIR}/src ${PROJECT_SOURCE_DIR}/Libraries/Shark/Include)
file(GLOB_RECURSE sources src/*.cpp src/*.c src/*.hpp)

option( ENABLE_CBLAS "Use Installed Linear Algebra Library" ON )
if( ENABLE_CBLAS )
	set(CBLAS_VENDOR "Accelerate")
	set(CBLAS_INCLUDES "")
	set(CBLAS_LIBRARIES "-framework Accelerate" )
    if(CBLAS_VENDOR)
		message(STATUS "CBLAS FOUND: " ${CBLAS_VENDOR} " with include directory " ${CBLAS_INCLUDES} )
		set(SHARK_USE_CBLAS 1)
		include_directories ( ${CBLAS_INCLUDES} )
	else()
		message(STATUS "No usable CBLAS Library found. No fast linear Algebra used.")
	endif()
endif()
add_executable(main ${sources})
find_package(Boost REQUIRED COMPONENTS serialization)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS} ${CBLAS_INCLUDES})
    target_link_libraries(main ${Boost_LIBRARIES} ${CBLAS_LIBRARIES} libshark.a)
endif()



